:- module mpm.project.dependency.
:- interface.
:- import_module list.
:- import_module json.

:- include_module mpm.project.dependency.source.
:- import_module mpm.project.dependency.source.



:- type dependency --->
     dependency(name :: string,
                source :: mpm.project.dependency.source.source).


:- func list_from_json(json.object::in) = (list(dependency)::out) is det.


:- func list_to_json(list(dependency)::in) = (json.value::out) is det.



:- implementation.
:- import_module maybe.
:- import_module list.
:- import_module map.


list_from_json(DependenciesObject) = DependenciesList :-
  map.foldl(list_from_json_acc, DependenciesObject, [], DependenciesList).


:- pred list_from_json_acc(string::in, json.value::in, list(dependency)::in, list(dependency)::out) is det.
list_from_json_acc(Name, DependencyObject, DependencyListIn, DependencyListOut) :-
  from_json(Name, DependencyObject, Dependency),
  list.append(DependencyListIn, [Dependency], DependencyListOut).



:- pred from_json(string::in, json.value::in, dependency::out) is det.
from_json(Name, Value, Dependency) :-
  Source = mpm.project.dependency.source.source_from_json(Value),
  Dependency = dependency(Name, Source).



list_to_json(DependencyList) = DependenciesValue :-
  map.init(EmptyMap),
  InitialValue = json.object(EmptyMap),
  list.foldl(to_json_acc, DependencyList, InitialValue, DependenciesValue).


:- pred to_json_acc(dependency::in, json.value::in, json.value::out) is det.
to_json_acc(Dependency, ValueIn, ValueOut) :-
  to_json(Dependency, JsonValue),
  MapIn = json.det_get_object(ValueIn),
  map.det_insert(Dependency ^ name, JsonValue, MapIn, MapOut),
  ValueOut = object(MapOut).


:- pred to_json(dependency::in, json.value::out) is det.
to_json(Dependency, JsonValue) :-
  JsonValue = mpm.project.dependency.source.source_to_json(Dependency ^ source).
