%%
%% This module defines the project type, and predicates for dealing with project structures.
%%
:- module mpm.project.
:- interface.
:- import_module io.
:- import_module list.
:- import_module maybe.

:- include_module mpm.project.dependency.
:- import_module mpm.project.dependency.




:- type project
   ---> project(name :: string,
                project_type :: project_type,
                version :: string,
                mercury_version :: string,
                direct_dependencies :: list(mpm.project.dependency.dependency),
                indirect_dependencies :: list(mpm.project.dependency.dependency),
                path :: string).

:- type project_type ---> application
                        ; library.




%%
%% Initilize a new project in the current working directory.
%% This DOES NOT initilize a new project type.
%%
%% Usage:
%%   mpm.project.init(!IO)
%%
:- pred init(io::di, io::uo) is det.



%%
%% Deserializes mpm.json in the current working directory, returning the
%% project structure in DeserializedProject.
%%
%% Usage:
%%    mpm.project.from_json_file(MpmJsonPath, DeserializedProject, !IO)
%%
:- pred from_json_file(string::in, project::out, io::di, io::uo) is det.



%%
%% Serializes ProjectToSerialize into mpm.json in the current
%% working directory.
%%
%% Usage:
%%   mpm.project.to_json_file(ProjectToSerialize, !IO)
%%
:- pred to_json_file(project::in, io::di, io::uo) is det.



%%
%% Determines the source path of the project
%%
%% Usage:
%%   mpm.project.src_path(Project, ProjectSrcPath)
%%
:- pred src_path(project::in, string::out) is det.


%%
%% Determines the mpm path for the project, this is where mpm
%% keeps all it's non-user editable data.
%%
%% Usage:
%%   mpm.project.src_path(Project, ProjectSrcPath)
%%
:- pred mpm_path(project::in, string::out) is det.


%%
%% Determines the package path for the project.   This is where all dependendy
%% packages are installed.
%%
%% Usage:
%%   mpm.project.src_path(Project, ProjectSrcPath)
%%
:- pred packages_path(project::in, string::out) is det.



%%
%% Computes the full path to the list of generated target files.
%% Library projects, in particular, may generate multiple target files.
%%
%% The returned list is a set of absolute paths to the generated target files.
%%
%% Usage:
%%   mpm.project.project_name(Project, TargetName)
%%
:- pred target_files(project::in, list(string)::out) is det.


:- implementation.
:- import_module io.
:- import_module list.
:- import_module string.
:- import_module maybe.
:- import_module stream.
:- import_module assoc_list.
:- import_module pair.
:- import_module map.

:- use_module json.
:- use_module dir.
:- use_module exception.


:- instance json.from_json(project) where [
  func(from_json/1) is project_from_json
].


:- instance json.to_json(project) where [
 func(to_json/1) is project_to_json
].


:- func project_from_json(json.value) = maybe.maybe_error(project).
project_from_json(Value) = MaybeMpmProject :-
  (
    if json.get_object(Value, Object)
    then marshal_project(Object, MaybeMpmProject)
    else MaybeMpmProject = maybe.error("File is not a valid project file")
  ).



:- func project_to_json(project) = json.value.
project_to_json(MpmProject) = JsonValue :-
  Dependencies = json.det_make_object([
    "direct" - mpm.project.dependency.list_to_json(MpmProject ^ direct_dependencies),
    "indirect" - mpm.project.dependency.list_to_json(MpmProject ^ indirect_dependencies)
  ]),

  JsonValue = json.det_make_object([
    "name" - json.to_json(MpmProject ^ name : string),
    "type" - json.to_json(project_type_to_string(MpmProject ^ project_type) : string),
    "version" - json.to_json(MpmProject ^ version : string),
    "mercury-version" - json.to_json(MpmProject ^ mercury_version : string),
    "dependencies" - Dependencies
  ]).



:- pred marshal_project(json.object::in, maybe.maybe_error(project)::out) is det.
marshal_project(Object, MaybeMpmProject) :-
    Name = json.search_string(Object, "name", ""),

    ProjectTypeString = json.search_string(Object, "type", "application"),

    ProjectType = project_type_from_string(ProjectTypeString),

    Version = json.search_string(Object, "version", "0.0.0"),

    MercuryVersion =
        json.search_string(Object, "mercury-version", "14.01.1"),

    map.init(EmptyObject),

    DependenciesObject =
            json.search_object(Object, "dependencies", EmptyObject),

    DirectDependenciesObject =
        json.search_object(DependenciesObject, "direct", EmptyObject),

    DirectDependencies =
        mpm.project.dependency.list_from_json(DirectDependenciesObject),

    IndirectDependenciesObject =
        json.search_object(DependenciesObject, "indirect", EmptyObject),

    IndirectDependencies =
        mpm.project.dependency.list_from_json(IndirectDependenciesObject),

    MpmProject = project(
        Name,
        ProjectType,
        Version,
        MercuryVersion,
        DirectDependencies,
        IndirectDependencies,
        "."),

    MaybeMpmProject = maybe.ok(MpmProject).




:- func project_type_from_string(string::in) = (project_type::out) is det.
project_type_from_string(ProjectTypeString) = ProjectType :-
    LowerProjectTypeString = string.to_lower(ProjectTypeString),
    (
        if project_type_from_string_inner(LowerProjectTypeString, PType)
        then ProjectType = PType
        else ProjectType = application
    ).

:- pred project_type_from_string_inner(
    string::in,
    project_type::out) is semidet.

project_type_from_string_inner(ProjectTypeString, ProjectType) :-
    (
        ProjectTypeString = "application",
        ProjectType = application
    ;
        ProjectTypeString = "library",
        ProjectType = library
    ).


:- func project_type_to_string(project_type::in) = (string::out) is det.
project_type_to_string(application) = "application".
project_type_to_string(library) = "library".




from_json_file(ProjectPath, MpmProject, !IO) :-
    ReaderParams = json.reader_params(
        json.allow_comments,
        json.allow_trailing_commas,
        json.allow_repeated_members_keep_last,
        json.allow_infinities,
        json.no_maximum_nesting_depth),

    mpm.file_system.relative_to_absolute_path(
        ProjectPath,
        AbsoluteProjectPath,
        !IO),

    MpmJsonPath = dir.make_path_name(AbsoluteProjectPath, "mpm.json"),
    io.open_input(MpmJsonPath, MaybeInputStream, !IO),

    (
        if MaybeInputStream = ok(IStream)
        then IStream = InputStream
        else exception.throw("Could not open mpm.json for reading")
    ),


   json.init_reader(InputStream, ReaderParams, Reader, !IO),
   json.read_value(Reader, JsonResult, !IO),

   (
       JsonResult = ok(JsonValue),
       MaybeMpmProject = project_from_json(JsonValue),
       (
           MaybeMpmProject = maybe.ok(MpmProjectNoPath),
           MpmProject = 'path :='(MpmProjectNoPath, AbsoluteProjectPath)
       ;
           MaybeMpmProject = maybe.error(ErrorString),
           ExceptionString = string.format("Error: %s", [s(ErrorString)]),
           exception.throw(ExceptionString)
       )
  ;
       JsonResult = stream.eof,
       exception.throw("mpm.json appears to be empty")
  ;
       JsonResult = error(JsonError),
       ErrorMessage = error_message(JsonError),
       ExceptionString = string.format("Error: %s", [s(ErrorMessage)]) : string,
       exception.throw(ExceptionString)
  ),

  io.close_input(InputStream, !IO).




to_json_file(MpmProject, !IO) :-
  WriterParams = json.writer_params(json.pretty,
                                    json.do_not_allow_infinities,
                                    json.do_not_escape_solidus,
                                    json.no_member_filter),

  ProjectPath = project.path(MpmProject),
  MpmJsonPath = dir.make_path_name(ProjectPath, "mpm.json"),
  io.open_output(MpmJsonPath, MaybeOutputStream, !IO),

  (
    if MaybeOutputStream = ok(OStream)
    then OStream = OutputStream
    else exception.throw("Could not open mpm.json for writing")
  ),


  json.init_writer(OutputStream, WriterParams, Writer, !IO),
  json.put_value(Writer, json.to_json(MpmProject), !IO),
  io.close_output(OutputStream, !IO).





init(!IO) :-
    io.format("Initilizing project\n", [], !IO),
    mpm.file_system.current_working_directory(ProjectPath, !IO),
    BaseName = dir.det_basename(ProjectPath),

    MpmProject = project(
        BaseName,
        application,
        "0.0.0",
        "14.01.1",
        [],
        [],
        ProjectPath),

    to_json_file(MpmProject, !IO),
    src_path(MpmProject, SrcPath),
    mpm.file_system.make_directory(SrcPath, !IO).


src_path(Project, ProjectSrcPath) :-
    ProjectPath = project.path(Project),
    ProjectSrcPath = dir.make_path_name(ProjectPath, "src").


mpm_path(Project, ProjectMpmPath) :-
    ProjectPath = project.path(Project),
    ProjectMpmPath = dir.make_path_name(ProjectPath, ".mpm").


packages_path(Project, ProjectPackagesPath) :-
    mpm_path(Project, ProjectMpmPath),
    ProjectPackagesPath = dir.make_path_name(ProjectMpmPath, "packages").



target_files(Project, TargetFileList) :-
    ProjectType = project_type(Project),
    ProjectName = name(Project),
    src_path(Project, SrcPath),
    (
        ProjectType = application,
        ApplicationTargetFile = dir.make_path_name(SrcPath, ProjectName),
        TargetFileList = [ApplicationTargetFile]
    ;
        ProjectType = library,
        string.append(ProjectName, ".so", SoFileName),
        string.append(ProjectName, ".a", ArchiveFileName),
        SoFilePath = dir.make_path_name(SrcPath, SoFileName),
        ArchiveFilePath = dir.make_path_name(SrcPath, ArchiveFileName),
        TargetFileList = [SoFilePath, ArchiveFilePath]
    ).
