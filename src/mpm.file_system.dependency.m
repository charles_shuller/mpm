:- module mpm.file_system.dependency.
:- interface.
:- import_module io.
:- use_module mpm.project.dependency.


%%
%% Determines the absolute path of a dependency, returning it in AbsoluteDependencyPath
%%
%% Usage:
%%    mpm.file_system.dependency.path(Dependency, AbsoluteDependencyPath, !IO)
%%
:- pred path(mpm.project.dependency.dependency::in, string::out, io::di, io::uo) is det.


%%
%% Determines the absolute path of a dependencies src directory, returning it in AbsoluteDependencySrcPath
%%
%% Usage:
%%    mpm.file_system.dependency.src_path(Dependency, AbsoluteDependencySrcPath, !IO)
%%
:- pred src_path(mpm.project.dependency.dependency::in, string::out, io::di, io::uo) is det.



%%
%% Creates a directory for the dependency if it doesn't already exist.
%%
%% Usage:
%%   mpm.file_system.dependency.make_directory(Dependency, !IO).
%%
:- pred make_directory(mpm.project.dependency.dependency::in, io::di, io::uo) is det.


%%
%% Fetches a dependency, this will typically be from a git repository.
%%
%% Usage:
%%   mpm.file_system.dependency.fetch(Dependency, !IO)
%%
:- pred fetch(mpm.project.dependency.dependency::in, io::di, io::uo) is det.



:- implementation.
:- import_module string.
:- use_module dir.
:- use_module mpm.throw.
:- use_module mpm.git.
:- use_module mpm.project.dependency.source.

path(Dependency, AbsoluteDependencyPath, !IO) :-
  DependencyName = mpm.project.dependency.name(Dependency),
  RelativeDependencyPath = dir.relative_path_name_from_components([".mpm", "packages", DependencyName]),
  mpm.file_system.relative_to_absolute_path(RelativeDependencyPath, AbsoluteDependencyPath, !IO).


src_path(Dependency, AbsoluteDependencySrcPath, !IO) :-
  path(Dependency, DependencyPath, !IO),
  AbsoluteDependencySrcPath = dir.make_path_name(DependencyPath, "src").



make_directory(Dependency, !IO) :-
  path(Dependency, DependencyPath, !IO),
  mpm.file_system.make_directory(DependencyPath, !IO).


:- pred fetch_inner(mpm.project.dependency.dependency::in, string::in, io::di, io::uo) is det.
fetch_inner(Dependency, DependencyPath, !IO) :-
  io.format("Fetching %s\n", [s(DependencyPath)], !IO),
  DependencySource = mpm.project.dependency.source(Dependency),
  mpm.project.dependency.source.source(SourceType, RepoUri, Version) = DependencySource,

  (
    SourceType = mpm.project.dependency.source.git,
    mpm.git.clone(RepoUri, DependencyPath, !IO),
    mpm.git.checkout(DependencyPath, Version, !IO)
  ).


fetch(Dependency, !IO) :-
  path(Dependency, DependencyPath, !IO),
  file_exists(DependencyPath, AlreadyHasDependency, !IO),

  (
    AlreadyHasDependency = yes,
    io.format("Dependency already exists, not re-fetching\n", [], !IO)
  ;
    AlreadyHasDependency = no,
    fetch_inner(Dependency, DependencyPath, !IO)
  ).
