:- module mpm.project.dependency.source.
:- interface.

:- type source ---> source(source_type :: source_type,
                           source :: string,
                           version :: string).

:- type source_type ---> git.


:- func source_from_json(json.value::in) = (source::out) is det.
:- func source_to_json(source::in) = (json.value::out) is det.



:- implementation.
:- import_module json.


source_from_json(Value) = Source :-
  Object = json.det_get_object(Value),
  StringSourceType = json.search_string(Object, "type", "git"),
  SourceType = source_type_from_string(StringSourceType),

  SourceString = json.search_string(Object, "source", ""),
  Version = json.search_string(Object, "version", ""),

  Source = source(SourceType, SourceString, Version).


source_to_json(Source) = JsonValue :-
  SourceTypeString = source_type_to_string(Source ^ source_type),

  JsonValue = json.det_make_object([
    "type" - to_json(SourceTypeString : string),
    "source" - to_json(Source ^ source : string),
    "version" - to_json(Source ^ version : string)
  ]).


:- func source_type_to_string(source_type::in) = (string::out) is det.
source_type_to_string(git) = "git".

:- func source_type_from_string(string::in) = (source_type::out) is det.
source_type_from_string(SourceTypeString) = SourceType :-
  (
    if source_type_from_string_inner(string.to_lower(SourceTypeString), SType)
    then SourceType = SType
    else SourceType = git
  ).

:- pred source_type_from_string_inner(string::in, source_type::out) is semidet.
source_type_from_string_inner(SourceTypeString, SourceType) :-
  (
    SourceTypeString = "git",
    SourceType = git
  ).
