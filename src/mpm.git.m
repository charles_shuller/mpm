%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module provides functions for interacting with git
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module mpm.git.
:- interface.
:- import_module io.

%%
%% Handles cloning a git repository
%%
%% Usage:
%%   clone_repository(RepoUri, DestinationPath, !IO) :-
%%
:- pred clone(string::in, string::in, io::di, io::uo) is det.

%%
%% Checks out a branch, tag, or commit in the repository found at LocalRepoPath.
%%
%% Usage:
%%   checkout(LocalRepoPath, BranchTagOrCommit, !IO)
%%
:- pred checkout(string::in, string::in, io::di, io::uo) is det.






:- implementation.
:- use_module string.
:- use_module mpm.system.

clone(RepoUri, DestinationPath, !IO) :-
  string.format("git clone %s %s", [s(RepoUri), s(DestinationPath)], CloneCommand),
  mpm.system.call_shell(CloneCommand, _CloneExitCode, !IO).


checkout(LocalRepoPath, BranchTagOrCommit, !IO) :-
  string.format("cd %s && git checkout %s", [s(LocalRepoPath), s(BranchTagOrCommit)], CheckoutCommand),
  mpm.system.call_shell(CheckoutCommand, _CloneExitCode, !IO).
