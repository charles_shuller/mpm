%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module deals with implementing the build command, in large part,
%% it is implemented in terms of functions found in mpm.file_system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module mpm.build.
:- interface.
:- import_module io.
:- use_module mpm.project.


%%
%% Builds Project.  First, it ensures all dependencies are downloaded and
%% built, then it builds the current project.
%%
%% Usage:
%%     build(Project, !IO)
%%
:- pred build(mpm.project.project::in, io::di, io::uo) is det.






:- implementation.
:- import_module list.
:- import_module bool.

:- use_module dir.
:- use_module string.

:- use_module mpm.project.dependency.
:- use_module mpm.project.dependency.source.
:- use_module mpm.file_system.
:- use_module mpm.file_system.dependency.
:- use_module mpm.git.
:- use_module mpm.throw.
:- use_module mpm.system.



:- type build_type --->  mpm
                   ;     make
                   ;     autoconf.





:- pred build_type_key_file_path(string::in,
                                 build_type::in,
                                 string::out,
                                 io::di,
                                 io::uo) is det.

build_type_key_file_path(DependencyPath, mpm, MpmFilePath, !IO) :-
    MpmFilePath = dir.make_path_name(DependencyPath, "mpm.json").

build_type_key_file_path(DependencyPath, make, MpmFilePath, !IO) :-
    MpmFilePath = dir.make_path_name(DependencyPath, "Makefile").

build_type_key_file_path(DependencyPath, autoconf, MpmFilePath, !IO) :-
    MpmFilePath = dir.make_path_name(DependencyPath, "configure").


:- pred has_project_key_file(string::in,
                             build_type::in,
                             bool::out,
                             io::di,
                             io::uo) is det.
has_project_key_file(DependencyPath, BuildType, HasKeyFile, !IO) :-
    build_type_key_file_path(DependencyPath, BuildType, FilePath, !IO),
    mpm.file_system.file_exists(FilePath, HasKeyFile, !IO).


:- pred is_mpm_project(string::in, bool::out, io::di, io::uo) is det.
is_mpm_project(DependencyPath, IsMpm, !IO) :-
    has_project_key_file(DependencyPath, mpm, IsMpm, !IO).


:- pred is_make_project(string::in, bool::out, io::di, io::uo) is det.
is_make_project(DependencyPath, IsMake, !IO) :-
    has_project_key_file(DependencyPath, make, HasMake, !IO),
    has_project_key_file(DependencyPath, autoconf, HasAutoConf, !IO),
    has_project_key_file(DependencyPath, mpm, HasMpm, !IO),
    NotHasAutoConf = not(HasAutoConf),
    NotHasMpm = not(HasMpm),
    IsMake = and_list([HasMake, NotHasAutoConf, NotHasMpm]).


:-pred is_autoconf_project(string::in, bool::out, io::di, io::uo) is det.
is_autoconf_project(DependencyPath, IsAutoConf, !IO) :-
    has_project_key_file(DependencyPath, autoconf, HasAutoConf, !IO),
    has_project_key_file(DependencyPath, mpm, HasMpm, !IO),
    NotHasMpm = not(HasMpm),
    IsAutoConf = and_list([HasAutoConf, NotHasMpm]).


:- pred guess_build_type_inner(string::in,
                               bool::in,
                               bool::in,
                               bool::in,
                               build_type::out) is det.
guess_build_type_inner(DependencyPath, IsMpm, IsMake, IsAutoConf, BuildType) :-
    (
        if IsMpm = yes
        then BuildType = mpm
        else
            if IsMake = yes
            then BuildType = make
            else
                if IsAutoConf = yes
                then BuildType = autoconf
                else
                    mpm.throw.message("Could not determine project type of %s",
                                      [s(DependencyPath)])
    ).

:- pred guess_build_type(string::in, build_type::out, io::di, io::uo) is det.
guess_build_type(DependencyPath, BuildType, !IO) :-
    is_mpm_project(DependencyPath, IsMpm, !IO),
    is_make_project(DependencyPath, IsMake, !IO),
    is_autoconf_project(DependencyPath, IsAutoConf, !IO),

    guess_build_type_inner(DependencyPath,
                           IsMpm,
                           IsMake,
                           IsAutoConf,
                           BuildType).


:- pred build_dependency(string::in, build_type::in, io::di, io::uo) is det.
build_dependency(DependencyPath, mpm, !IO) :-
    mpm.project.from_json_file(DependencyPath, Project, !IO),
    build(Project, !IO).

build_dependency(DependencyPath, make, !IO) :-
    string.format("cd %s && make", [s(DependencyPath)], BuildCommand),
    mpm.system.call_shell(BuildCommand, _ExitCode, !IO).

build_dependency(DependencyPath, autoconf, !IO) :-
    string.format("cd %s && ./configure && make",
                  [s(DependencyPath)],
                  BuildCommand),
    mpm.system.call_shell(BuildCommand, _ExitCode, !IO).





:- pred src_dir_list(string::in, mpm.project.dependency.dependency::in,
                     list(string)::in,
                     list(string)::out,
                     io::di,
                     io::uo) is det.
src_dir_list(Flag, Dependency, SearchDirListIn, SearchDirListOut, !IO) :-
    mpm.file_system.dependency.src_path(Dependency, DependencySrcPath, !IO),
    string.format("%s %s", [s(Flag), s(DependencySrcPath)], SearchLibDir),
    list.append(SearchDirListIn, [SearchLibDir], SearchDirListOut).


:- pred prepend_flag(string::in,
                     string::in,
                     list(string)::in,
                     list(string)::out) is det.
prepend_flag(Flag, Arg, ArgsWithFlagIn, ArgsWithFlagOut) :-
    ArgWithFlag = string.join_list(" ", [Flag, Arg]),
    list.append(ArgsWithFlagIn, [ArgWithFlag], ArgsWithFlagOut).

:- pred args_from_dep_src_files(pred(string, list(string), io, io),
                                string,
                                mpm.project.dependency.dependency,
                                list(string),
                                list(string),
                                io,
                                io).
:- mode args_from_dep_src_files(pred(in, out, di, uo) is det,
                                in,
                                in,
                                in,
                                out,
                                di,
                                uo) is det.
args_from_dep_src_files(FileListPred,
                        Flag,
                        Dependency,
                        FileListIn,
                        FileListOut,
                        !IO) :-
    mpm.file_system.dependency.src_path(Dependency, DepSrcPath, !IO),
    FileListPred(DepSrcPath, DepFileList, !IO),
    list.foldl(prepend_flag(Flag), DepFileList, [], DepFileListWithFlags),
    list.append(FileListIn, DepFileListWithFlags, FileListOut).



:- pred libs_in_dep_src(string::in,
                        mpm.project.dependency.dependency::in,
                        list(string)::in,
                        list(string)::out,
                        io::di,
                        io::uo) is det.
libs_in_dep_src(Flag, Dependency, LibListIn, LibListOut, !IO) :-
    args_from_dep_src_files(mpm.file_system.library_files_in_dir,
                            Flag,
                            Dependency,
                            LibListIn,
                            LibListOut,
                            !IO).


:- pred init_files_in_dep_src(string::in,
                              mpm.project.dependency.dependency::in,
                              list(string)::in,
                              list(string)::out,
                              io::di,
                              io::uo) is det.
init_files_in_dep_src(Flag,
                      Dependency,
                      InitFileListIn,
                      InitFileListOut,
                      !IO) :-
    args_from_dep_src_files(mpm.file_system.init_files_in_dir,
                            Flag,
                            Dependency,
                            InitFileListIn,
                            InitFileListOut,
                            !IO).



:- pred dependency_args(mpm.project.project,
                        pred(mpm.project.dependency.dependency,
                             list(string),
                             list(string),
                             io,
                             io),
                        string,
                        io,
                        io).

:- mode dependency_args(in,
                        pred(in,
                             in,
                             out,
                             di,
                             uo) is det,
                        out,
                        di,
                        uo) is det.

dependency_args(Project, FlagValuePred, Args, !IO) :-
    IndirectDependencies = mpm.project.indirect_dependencies(Project),
    DirectDependencies = mpm.project.direct_dependencies(Project),
    list.append(IndirectDependencies, DirectDependencies, AllDepsList),

    list.foldl2(FlagValuePred, AllDepsList, [], ArgList, !IO),
    Args = string.join_list(" ", ArgList).




:- pred search_lib_files_dir_args(mpm.project.project::in,
                                  string::out,
                                  io::di,
                                  io::uo) is det.

search_lib_files_dir_args(Project, SearchLibFileDirs, !IO) :-
    dependency_args(Project,
                    src_dir_list("--search-lib-files-dir"),
                    SearchLibFileDirs,
                    !IO).



:- pred init_file_args(mpm.project.project::in,
                       string::out,
                       io::di,
                       io::uo) is det.
init_file_args(Project, InitFiles, !IO) :-
    dependency_args(Project,
                    init_files_in_dep_src("--init-file"),
                    InitFiles,
                    !IO).




:- pred link_object_args(mpm.project.project::in,
                         string::out,
                         io::di,
                         io::uo) is det.

link_object_args(Project, LinkObjectArgs, !IO) :-
    dependency_args(Project,
                    libs_in_dep_src("--link-object"),
                    LinkObjectArgs,
                    !IO).

:- pred target_name(mpm.project.project::in, string::out) is det.
target_name(Project, TargetName) :-
    ProjectName = mpm.project.name(Project),
    ProjectType = mpm.project.project_type(Project),
    (
        ProjectType = mpm.project.application,
        TargetName = ProjectName
    ;
        ProjectType = mpm.project.library,
        string.append("lib", ProjectName, TargetName)
    ).


:- pred build_project(mpm.project.project::in, io::di, io::uo) is det.
build_project(Project,  !IO) :-
    search_lib_files_dir_args(Project, SearchLibFilesDirs, !IO),
    init_file_args(Project, InitFiles, !IO),
    link_object_args(Project, LinkObjects, !IO),
    target_name(Project, TargetName),
    mpm.project.src_path(Project, SrcPath),
    ProjectName = mpm.project.name(Project),

    string.format("cd %s && mmc %s %s %s --make %s",
        [
         s(SrcPath),
         s(SearchLibFilesDirs),
         s(InitFiles),
         s(LinkObjects),
         s(TargetName)
        ],
        BuildCommand),

    io.format("BuildCommand: %s\n", [s(BuildCommand)], !IO),
    io.call_system(BuildCommand, Res, !IO),
    (
        Res = ok(_Status)
    ;
        Res = error(Error),
        mpm.throw.io_error("Could not build project %s",
                           [s(ProjectName)],
                           Error)
    ).




:- pred build_dependency(mpm.project.dependency.dependency::in,
                         io::di,
                         io::uo) is det.

build_dependency(Dependency, !IO) :-
    mpm.file_system.dependency.fetch(Dependency, !IO),
    mpm.file_system.dependency.path(Dependency, DependencyPath, !IO),
    guess_build_type(DependencyPath, BuildType, !IO),
    build_dependency(DependencyPath, BuildType, !IO).



:- pred build_dependencies(list(mpm.project.dependency.dependency)::in,
                           io::di,
                           io::uo) is det.

build_dependencies([], !IO) :- true.
build_dependencies([Dependency | RemainingDependencies], !IO) :-
    build_dependency(Dependency, !IO),
    build_dependencies(RemainingDependencies, !IO).



build(Project, !IO) :-
    DirectDependencies = mpm.project.direct_dependencies(Project),
    build_dependencies(DirectDependencies, !IO),
    build_project(Project, !IO).
