%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module contains predicates used to facilitate easy conversion of
%% various errors into an exception, and throw it.
%%
%% In general, mpm never attempts to recover from an error, and these
%% exceptions are used to create a non-local exit, with a message.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module mpm.throw.
:- interface.
:- import_module string.

:- type arg_list == list(string.poly_type).

%%
%% Converts an io.error Error into a string, appends it to Message, then throws
%% the resultant string as an exception.  The colon and whitespace are added by
%% the function.
%%
%% For example:
%%     io_error("Could not determine if file %s exists", [s(FileName)], Error)
%%
%% will produce the exception like:
%%     exception.throw("Could not determine if file exists: Access Denied")
%%
%% Usage:
%%   mpm.throw.io_error("Encountered an error while processing file %s",
%%                      [s(FileName)],
%%                      IoError)
%%
:- pred io_error(string::in, arg_list::in, io.error::in) is erroneous.




%%
%% Formats Message and Args with string.format, and raises the resultant string
%% as an exception.
%%
%% Usage:
%%   mpm.throw.message("An error happend processing file %s",
%%                     [s(FileName)],
%%                     !IO)
%%
:- pred message(string::in, arg_list::in) is erroneous.




:- implementation.
:- use_module exception.



message(MessageFormatString, ArgList) :-
    string.format(MessageFormatString, ArgList, ErrorMessage),
    exception.throw(ErrorMessage).



io_error(MessageFormatString, MessageArgList, Error) :-
    io.error_message(Error, ErrorString),
    string.append(MessageFormatString, ": %s", FormatString),
    list.append(MessageArgList, [s(ErrorString)], ArgList),
    message(FormatString, ArgList).
