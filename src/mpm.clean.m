%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module deals with implementing the clean command, in large part,
%% it is implemented in terms of functions found in mpm.file_system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module mpm.clean.
:- interface.
:- import_module io.
:- use_module mpm.project.


%%
%% Cleans the constructed files for THIS project
%%
:- pred clean(mpm.project.project::in, io::di, io::uo) is det.


%%
%% Cleans the constructed files for this project AND it's dependencies
%%
:- pred real_clean(mpm.project.project::in, io::di, io::uo) is det.




:- implementation.
:- import_module bool.
:- import_module list.
:- use_module dir.

:- use_module mpm.system.
:- use_module mpm.file_system.
:- use_module mpm.throw.


:- pred remove_packages_directory(string::in, io::di, io::uo) is det.
remove_packages_directory(PackagesDir, !IO) :-
    mpm.file_system.remove_file_recursively(PackagesDir, !IO).



:- pred remove_mercury_directory(string::in, io::di, io::uo) is det.
remove_mercury_directory(SrcPath, !IO) :-
    MercuryDir = dir.make_path_name(SrcPath, "Mercury"),
    mpm.file_system.remove_file_recursively(MercuryDir, !IO).




:- pred remove_generated_auxillary_file(string::in,
                                        string::in,
                                        io.file_type::in,
                                        bool::out,
                                        int::in,
                                        int::out,
                                        io::di,
                                        io::uo) is det.

remove_generated_auxillary_file(DirName,
                                BaseName,
                                _FileType,
                                Continue,
                                AccIn,
                                AccOut,
                                !IO) :-
    AccIn = AccOut,
    Continue = yes,
    FileAbsPath = dir.make_path_name(DirName, BaseName),
    SplitString = string.split_at_char('.', BaseName),
    list.reverse(SplitString, ReversedSplitString),
    FileExtension = list.det_head(ReversedSplitString),
    (
        if   list.member(FileExtension, ["mh", "err"])
        then mpm.file_system.remove_file(FileAbsPath, !IO)
        else true
    ).



:- pred remove_generated_auxillary_files(string::in, io::di, io::uo) is det.
remove_generated_auxillary_files(SrcPath, !IO) :-
    dir.foldl2(
        remove_generated_auxillary_file,
        SrcPath,
        0,
        MaybePartialRes,
        !IO),
    (
        MaybePartialRes = ok(_)
    ;
        MaybePartialRes = error(_PartialFileList, Error),

        mpm.throw.io_error(
            "Error encountered removing generated auxiallary files in %s",
            [s(SrcPath)],
            Error)
    ).


:- pred remove_project_targets(mpm.project.project::in, io::di, io::uo) is det.
remove_project_targets(Project, !IO) :-
    mpm.project.target_files(Project, TargetFileList),
    list.foldl(mpm.file_system.remove_file, TargetFileList, !IO).


clean(Project, !IO) :-
    mpm.project.src_path(Project, SrcPath),
    remove_mercury_directory(SrcPath, !IO),
    remove_generated_auxillary_files(SrcPath, !IO),
    remove_project_targets(Project, !IO).


real_clean(Project, !IO) :-
    clean(Project, !IO),
    mpm.project.packages_path(Project, PackagesPath),
    remove_packages_directory(PackagesPath, !IO).
