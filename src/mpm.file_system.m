%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module contains file system interaction predicates used by mpm.  We use this mainly to limit dependencies in the
%% top level project, but additionally to trap errors, and raise them as exceptions.  We do not catch these exceptions,
%% and expect the program to terminate.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module mpm.file_system.
:- interface.

:- include_module mpm.file_system.dependency.

:- import_module io.
:- import_module string.
:- import_module list.
:- import_module bool.



%%
%% This predicate checks for file existance, since the current stable version
%% of mercury does not currently provide this functionality.
%%
%% Usage:
%%       file_exists(Path, FileExists, !IO)
%%
%% Since the predicate accepts IO state variables, it is not allowed to be
%% semi-det, so it's result is returned via the second parameter.
%%
:- pred file_exists(string::in, bool::out, io::di, io::uo) is det.


%%
%% This predicate examines a directory tree for generated library files,
%% Returning them as a list in the second parameter.
%%
%% The filenames in LibararyFileList are absolute paths to the files
%%
%% Usage:
%%       library_files_in_dir(Path, LibraryFileList, !IO)
%%
%%
%%
:- pred library_files_in_dir(
    string::in,
    list(string)::out,
    io::di,
    io::uo) is det.


%%
%% This predicate examines a directory tree for generated init files,
%% Returning them as a list in the second parameter.
%%
%% The filenames in InitFileList are absolute paths to the files
%%
%% Usage:
%%       init_files_in_dir(Path, LibraryFileList, !IO)
%%
%%
%%
:- pred init_files_in_dir(
    string::in,
    list(string)::out,
    io::di,
    io::uo) is det.



%%
%% This predicate creates a directory in the file system
%%
%% Usage:
%%     mpm.file_system.make_directory(Path, !IO)
%%
:- pred make_directory(string::in, io::di, io::uo) is det.





%%
%% This predicate returns the current working direcory.  On error, it
%% raises an exception.
%%
%% Usage:
%%   current_working_directory(CurrentWorkingDirecotry, !IO)
%%
:- pred current_working_directory(string::out, io::di, io::uo) is det.



%%
%% Extracts the dirctory part of FilePath, ensures it is an absolute path, then
%% unifies the absolute directory part of FilePath with AbsoluteDirectoryOfFile
%%
%% Usage:
%%   absolute_dirname(FilePath, AbsoluteDirectoryOfFile, !IO)
%%
:- pred absolute_dirname(string::in, string::out, io::di, io::uo) is det.




%%
%% If MaybeRelativePath is an absolute path, then
%% AbosolutePath = MaybeRelativePath Otherwise, Converts MaybeRelativePath to
%% an absolute path, by prepending the current working directory to it
%%
%% Usage:
%    relative_to_absolute_path(MaybeRelativePath, AbsolutePath, !IO)
%%
:- pred relative_to_absolute_path(
    string::in,
    string::out,
    io::di,
    io::uo) is det.



%%
%% This predicate is exactly like io.remove_file, except it raises an exception
%% on error.
%%
%% Usage:
%%   remove_file(Path, !IO)
%%
:- pred remove_file(string::in, io::di, io::uo) is det.


%%
%% This predicate is exactly like io.remove_file_recursively, except it raises
%% an exception on error
%%
%% Usage:
%%   remove_file_recursively(Path, !IO)
%%
:- pred remove_file_recursively(string::in, io::di, io::uo) is det.




:- implementation.
:- use_module exception.
:- use_module dir.
:- use_module mpm.throw.

%%
%% We call out to the C Standard library to check errno for this.  If an
%% unexpected error occurrs we report it in the ErrorOccurred parameter, a
%% subsequent call to io.file_type and io.check_file_accessability should be
%% made to find the actual error, that predicate does not currently report a
%% file that doesn't exist.
%%
:- pred file_exists_inner(
    string::in,
    bool::out,
    bool::out,
    io::di,
    io::uo) is det.
:- pragma foreign_proc(
    "C",
    file_exists_inner(
        Path::in,
        DoesExist::out,
        ErrorOccurred::out,
        IOin::di,
        IOout::uo),
    [promise_pure, will_not_call_mercury, thread_safe, tabled_for_io],
"
  struct stat stat_struct;
  int res = 0;

  do {
    errno = 0;
    res = stat(Path, &stat_struct);
  } while (res == -1 && MR_is_eintr(errno));

  switch(errno)
    {
    case 0:
      {
        DoesExist = MR_YES;
        ErrorOccurred = MR_NO;
        break;
      }
    case ENOENT:
      {
        DoesExist = MR_NO;
        ErrorOccurred = MR_NO;
        break;
      }
    case ENOTDIR:
      {
        DoesExist = MR_NO;
        ErrorOccurred = MR_NO;
        break;
      }
    default:
      {
        DoesExist = MR_NO;
        ErrorOccurred = MR_YES;
        break;
      }
    }

    IOin = IOout;
").


%%
%% Handles throwing an error for failed file stat calls.  Instead of moving a
%% lot of information from the C code, we just detect that an error happened,
%% then call io.file_type to find the error, and throw that error if we found
%% it.
%%
:- pred throw_stat_error(string::in, io::di, io::uo) is erroneous.
throw_stat_error(Path, !IO) :-
    io.file_type(yes, Path, Res, !IO),

    (
        Res = ok(_),
        string.format(
            "File exists failed with an unknown error for file: %s\n",
            [s(Path)],
            ErrorMessage),
        exception.throw(ErrorMessage)
    ;
        Res = error(Error),
        mpm.throw.io_error(
            "File exists failed for file %s, with error",
            [s(Path)],
            Error)
    ).


file_exists(Path, DoesExist, !IO) :-
    io.format("Checking for %s\n", [s(Path)], !IO),
    file_exists_inner(Path, DoesExist, ErrorOccurred, !IO),
    (
        if ErrorOccurred = yes
        then throw_stat_error(Path, !IO)
        else true
    ).



%%
%% This predicate searches a directory for library files.  Ultimately, it winds
%% up being a predicate passed to dir.foldl2, and as such is of type
%% dir.foldl_pred(list(string)) Please see dir.foldl2 documentation for more
%% information.
%%
%% This and the search_dir_for_init_files predicate are separate as it is
%% expected them to diverge substantially when windows support is added.
%%
%%
%% DirName:  Current directory being searched
%%
%% FileBaseName:  Name of the file, without the directory information
%%
%% FileType:  Type of file, we ignore it
%%
%% Continue: This MUST be yes to search the entire directory, setting it to no
%%           stops the search
%%
%% LibListIn: This is the Input Accumulator Variable, and is a list of
%%            libraries
%%
%% LibListOut: This is the Output Accumulator Variable, and is the updated list
%%             of libraries.
%%
%% !IO:  The IO state variables
%%
:- pred search_dir_for_libs(
    string::in,
    string::in,
    io.file_type::in,
    bool::out,
    list(string)::in,
    list(string)::out,
    io::di,
    io::uo) is det.

search_dir_for_libs(
    DirName,
    FileBaseName,
    _FileType,
    Continue,
    LibListIn,
    LibListOut,
    !IO) :-

    AbsFilePath = dir.make_path_name(DirName, FileBaseName),
    Continue = yes,

    (
        if string.suffix(FileBaseName, ".a")
        then list.append(LibListIn, [AbsFilePath], LibListOut)
        else LibListIn = LibListOut
    ).



%%
%% This predicate searches a directory for mercury init files.   Ultimately, it winds up being a predicate passed to dir.foldl2, and
%% as such is of type dir.foldl_pred(list(string))  Please see dir.foldl2 documentation for more information.
%%
%% This and the search_dir_for_libs predicate are separate as it is expected them to diverge substantially when windows support is added.
%%
%%
%% DirName:  Current directory being searched
%%
%% FileBaseName:  Name of the file, without the directory information
%%
%% FileType:  Type of file, we ignore it
%%
%% Continue: This MUST be yes to search the entire directory, setting it to no
%%           stops the search
%%
%% LibListIn: This is the Input Accumulator Variable, and is a list of
%%            libraries
%%
%% LibListOut: This is the Output Accumulator Variable, and is the updated list
%%             of libraries.
%%
%% !IO:  The IO state variables
%%
:- pred search_dir_for_init_files(
    string::in,
    string::in,
    io.file_type::in,
    bool::out,
    list(string)::in,
    list(string)::out,
    io::di,
    io::uo) is det.

search_dir_for_init_files(
    DirName,
    FileBaseName,
    _FileType,
    Continue,
    InitListIn,
    InitListOut,
    !IO) :-

    AbsFilePath = dir.make_path_name(DirName, FileBaseName),
    Continue = yes,

    (
        if   string.suffix(FileBaseName, ".init")
        then list.append(InitListIn, [AbsFilePath], InitListOut)
        else InitListIn = InitListOut
    ).



%%
%% This predicate is used primarily to deal with the error handling
%% necessitated by searching a directory for files with dir.fold2 It invokes
%% dir.fold2 with SearchPred, and if an error is detected, it raises it as an
%% exception.
%%
:- pred files_in_dir(
    pred(
        string,
        string,
        io.file_type,
        bool,
        list(string),
        list(string),
        io,
        io),
    string,
    list(string),
    io,
    io).

:- mode files_in_dir(
    pred(
        in,
        in,
        in,
        out,
        in,
        out,
        di,
        uo) is det,
    in,
    out,
    di,
    uo) is det.

files_in_dir(SearchPred, Path, FileList, !IO) :-
    dir.foldl2(SearchPred, Path, [], FileListRes, !IO),
    (
        FileListRes = ok(FileList)
    ;
        FileListRes = error(_PartialFileList, Error),
        mpm.throw.io_error(
            "Error encountered searching for files in %s",
            [s(Path)],
            Error)
  ).



library_files_in_dir(Path, LibFileList, !IO) :-
    files_in_dir(search_dir_for_libs, Path, LibFileList, !IO).


init_files_in_dir(Path, LibFileList, !IO) :-
    files_in_dir(search_dir_for_init_files, Path, LibFileList, !IO).




make_directory(Path, !IO) :-
    dir.make_directory(Path, Res, !IO),
    (
        Res = ok
    ;
        Res = error(Error),
        mpm.throw.io_error("Could not create directory %s", [s(Path)], Error)
    ).


current_working_directory(CurrentWorkingDirectory, !IO) :-
    dir.current_directory(PathRes, !IO),
    (
        PathRes = ok(CurrentWorkingDirectory)
    ;
        PathRes = error(Error),
        mpm.throw.io_error("Could not determine current working directory", [], Error)
    ).




absolute_dirname(FilePath, AbsoluteDirectoryOfFile, !IO) :-
    dir.dirname(FilePath, MaybeRelativeDir),
    relative_to_absolute_path(MaybeRelativeDir, AbsoluteDirectoryOfFile, !IO).



relative_to_absolute_path(MaybeRelativePath, AbsolutePath, !IO) :-
    (
        if dir.path_name_is_absolute(MaybeRelativePath)
        then AbsolutePath = MaybeRelativePath
        else current_working_directory(CurrentWorkingDirectory, !IO),
             AbsolutePath = dir.make_path_name(CurrentWorkingDirectory, MaybeRelativePath)
    ).


remove_file(Path, !IO) :-
    file_exists(Path, DoesExist, !IO),
    (
        DoesExist = yes,
        remove_file_inner(Path, !IO)
    ;
        DoesExist = no,
        true
    ).


:- pred remove_file_inner(string::in, io::di, io::uo) is det.
remove_file_inner(Path, !IO) :-
    io.remove_file(Path, Res, !IO),
    (
        Res = ok
    ;
        Res = error(Error),
        mpm.throw.io_error("Could not remove %s", [s(Path)], Error)
    ).




remove_file_recursively(Path, !IO) :-
    file_exists(Path, DoesExist, !IO),
    (
        DoesExist = yes,
        remove_file_recursively_inner(Path, !IO)
    ;
        DoesExist = no,
        true
    ).

:- pred remove_file_recursively_inner(string::in, io::di, io::uo) is det.
remove_file_recursively_inner(Path, !IO) :-
    io.remove_file_recursively(Path, Res, !IO),
    (
        Res = ok
    ;
        Res = error(Error),
        mpm.throw.io_error("Could not recursively remove %s", [s(Path)], Error)
    ).
