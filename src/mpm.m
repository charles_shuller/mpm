:- module mpm.
:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.





:- implementation.
:- import_module int.
:- import_module list.
:- import_module string.
:- import_module maybe.

:- include_module mpm.help.
:- include_module mpm.commands.
:- include_module mpm.config.
:- include_module mpm.project.
:- include_module mpm.build.
:- include_module mpm.file_system.
:- include_module mpm.throw.
:- include_module mpm.system.
:- include_module mpm.git.
:- include_module mpm.clean.

:- use_module mpm.commands.
:- use_module mpm.help.
:- use_module mpm.project.
:- use_module mpm.build.
:- use_module mpm.clean.
:- use_module mpm.file_system.


main(!IO) :-
  mpm.file_system.current_working_directory(ProjectPath, !IO),
  mpm.commands.from_command_line(MaybeCommand, !IO),
  (
    MaybeCommand = yes(mpm.commands.build),
    mpm.project.from_json_file(ProjectPath, Project, !IO),
    mpm.build.build(Project, !IO)
  ;
    MaybeCommand = yes(mpm.commands.help),
    mpm.help.print_usage(!IO)
  ;
    MaybeCommand = yes(mpm.commands.version),
    mpm.help.print_version(!IO)
  ;
    MaybeCommand = yes(mpm.commands.init),
    mpm.project.init(!IO)
  ;
    MaybeCommand = yes(mpm.commands.clean),
    mpm.project.from_json_file(ProjectPath, Project, !IO),
    mpm.clean.clean(Project, !IO)
  ;
    MaybeCommand = yes(mpm.commands.real_clean),
    mpm.project.from_json_file(ProjectPath, Project, !IO),
    mpm.clean.real_clean(Project, !IO)
  ;
    MaybeCommand = no,
    io.format("Unknown Command\n", [], !IO),
    mpm.help.print_usage(!IO)
  ).
