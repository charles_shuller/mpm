%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This module exports predicates for dealing with calling the underlying operating system.  Especially functions
%% like exec in the C library which actually pass a command off to the shell for further interpretation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module mpm.system.
:- interface.
:- import_module io.


%%
%% Execs Command in a shell, returning it's exit code in ExitCode.  This predicate primarily
%% exists to detect error conditions, wrap them as exceptions, and throw them to exit the program.
%% If you are making calls which may fail in a way that recovery is possible, please use
%% io.call_system/4 directly.
%%
%% Usage:
%%   call_shell(Command, ExitCode, !IO)
%%
:- pred call_shell(string::in, int::out, io::di, io::uo) is det.



:- implementation.
:- use_module mpm.throw.

call_shell(Command, ExitCode, !IO) :-
  io.call_system(Command, Res, !IO),
  (
    Res = ok(ExitCode)
  ;
    Res = error(Error),
    mpm.throw.io_error("Could not exec %s", [s(Command)], Error)
  ).
