Mercury Project Manager Roadmap
===============================================================================

Commands that need implementing
-------------------------------------------------------------------------------

*  mpm install mercury -- installs a current version of the mercury compiler
   (this will take a while)
*  mpm install -- processes the mpm.json file and installs any needed
   dependencies
*  mpm install <package_name> installs the needed mercury package local to the
   project




Functionality that needs implementing



mpm Packages
-------------------------------------------------------------------------------
Mercury Project Manager packages are really just git repositories of projects
managed by mpm.

Generic Make Suport:  The project is not managed by mpm, but is built with
                      "make".  This only works if the link and compile
                      arguments can be easily found.  mpm cannot automatically
                      resolve dependencies in this case.

Autoconf Support:  The project is not managed by mpm, but instead with the GNU
                   auto-tools.  In such a case, mpm can run configure, make,
                   make install, with appropriate arguments to configure, but
                   cannot automatically resolve dependencies.

MMC Support:  The project is not managed by mpm, but can be built with
              mmc --make <projectname>.


mpm Directory Layout
-------------------------------------------------------------------------------
mpm projects should normally be organized as:

${PROJECT_ROOT}
     mpm.json

${PROJECT_ROOT}/src
     mercury_source_files.m

${PROJECT_ROOT}/.mpm
     Files needed by mpm

${PROJECT_ROOT}/.mpm/packages
     This is where we download packages and build them.   Each in their own
     directory.


mpm Project Naming
-------------------------------------------------------------------------------
No nomenclature rules are followed.


mpm Project Versioning
-------------------------------------------------------------------------------
Projects are encouraged to use semver, though this is not enforced.

mpm DOES NOT allow "fuzzy" version matches as npm does.  The exact version
required must be specified.

Deployed packages MUST be


mpm.json
-------------------------------------------------------------------------------
Why JSON?  Mostly because lots of people run away from XML without any thought,
and YAML gives people grief with it's white-space sensitivity.

Sample application (i.e. something we can execute directly)

{
   "name": "test_application",
   "type": "application",
   "version": "1.0.0",
   "mercury-version": "14.01.1",
   "dependencies": {
      "direct": {
         "mpm/sample_package": "1.0.0",
         "extras/posix": "mercury-version",
         "external_sample_package": {"source": "https://some_git_repo.org",
                                     "type": "git",
                                     "version": "1.0.0"}
      },
      "indirect": {
         "mpm/required_by_sample_package": "1.0.0"
      }
   }
}


Sample library

{
   "name": "test_library",
   "type": "library",
   "mercury-version": "14.01.1",
}



Other fields

"linkage": "static" or "shared" (default is "static")
"grade": <any valid grade> (default is whatever mmc's default is, probably hlc)



mpm.org
------------------------------------------------------------------------------------------------------------------------
The website that hosts packages.