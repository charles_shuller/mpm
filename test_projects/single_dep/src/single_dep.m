:- module single_dep.
:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.






:- implementation.
:- import_module char.
:- import_module string.
:- import_module list.
:- import_module json.

main(!IO) :-
  _ = json.writer_params(pretty, do_not_allow_infinities, do_not_escape_solidus, no_member_filter),
  io.format("single_dep works!\n", [], !IO).
