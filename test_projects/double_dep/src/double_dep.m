:- module double_dep.
:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.






:- implementation.
:- import_module char.
:- import_module string.
:- import_module list.
:- import_module json.
:- import_module mpm_sample_project.

main(!IO) :-
  _ = json.writer_params(pretty, do_not_allow_infinities, do_not_escape_solidus, no_member_filter),
  (
    if mpm_sample_project.exported_predicate(1, 1)
    then io.format("double_dep works!\n", [], !IO)
    else io.format("double_dep works, but exported_predicate fails!\n", [], !IO)
  ).
